package com.n26.service;

import com.n26.model.Statistics;
import com.n26.model.Transaction;
import org.junit.Test;

import java.math.BigDecimal;

import static java.time.LocalDateTime.now;
import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.asList;
import static org.assertj.core.api.Assertions.*;

public class StatisticsServiceTest {

    @Test
    public void should_getEmptyStatistics_when_noTransactionExist() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        //when
        Statistics statistics = statisticsService.get();
        //then
        assertThat(statistics.getCount().doubleValue()).isEqualTo(0.0);
        assertThat(statistics.getSum().doubleValue()).isEqualTo(0.0);
        assertThat(statistics.getMax().doubleValue()).isEqualTo(0.0);
        assertThat(statistics.getMin().doubleValue()).isEqualTo(0.0);
        assertThat(statistics.getAvg().doubleValue()).isEqualTo(0.0);
    }

    @Test
    public void should_getStatistics_when_existValidTransaction() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        statisticsService.add(new Transaction().setAmount(new BigDecimal("122.455")).setTimestamp(now(UTC)));
        //when
        Statistics statistics = statisticsService.get();
        //then
        assertThat(statistics.getCount().doubleValue()).isEqualTo(1);
        assertThat(statistics.getSum().doubleValue()).isEqualTo(122.455);
        assertThat(statistics.getMax().doubleValue()).isEqualTo(122.455);
        assertThat(statistics.getMin().doubleValue()).isEqualTo(122.455);
        assertThat(statistics.getAvg().doubleValue()).isEqualTo(122.46);
    }

    @Test
    public void should_getStatistics_when_existValidTransactionsWithDifferentTimestamp() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        asList(new Transaction().setAmount(new BigDecimal("33.45")).setTimestamp(now(UTC)),
                new Transaction().setAmount(new BigDecimal("98.15")).setTimestamp(now(UTC).minusSeconds(5))
        ).forEach(statisticsService::add);
        //when
        Statistics statistics = statisticsService.get();
        //then
        assertThat(statistics.getCount().doubleValue()).isEqualTo(2);
        assertThat(statistics.getSum().doubleValue()).isEqualTo(131.60);
        assertThat(statistics.getMax().doubleValue()).isEqualTo(98.15);
        assertThat(statistics.getMin().doubleValue()).isEqualTo(33.45);
        assertThat(statistics.getAvg().doubleValue()).isEqualTo(65.80);
    }

    @Test
    public void should_getStatistics_when_existValidTransactionsWithSameTimestamp() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        asList(new Transaction().setAmount(new BigDecimal("11.41")).setTimestamp(now(UTC)),
                new Transaction().setAmount(new BigDecimal("36.10")).setTimestamp(now(UTC))
        ).forEach(statisticsService::add);
        //when
        Statistics statistics = statisticsService.get();
        //then
        assertThat(statistics.getCount().doubleValue()).isEqualTo(2);
        assertThat(statistics.getSum().doubleValue()).isEqualTo(47.51);
        assertThat(statistics.getMax().doubleValue()).isEqualTo(36.10);
        assertThat(statistics.getMin().doubleValue()).isEqualTo(11.41);
        assertThat(statistics.getAvg().doubleValue()).isEqualTo(23.755);
    }

    @Test
    public void should_getStatistics_when_existTransactionsWithInvalidTimestamp() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        asList(new Transaction().setAmount(new BigDecimal("11.41")).setTimestamp(now(UTC)),
                new Transaction().setAmount(new BigDecimal("36.10")).setTimestamp(now(UTC)),
                new Transaction().setAmount(new BigDecimal("99.99")).setTimestamp(now(UTC).minusSeconds(65)),
                new Transaction().setAmount(new BigDecimal("45.22")).setTimestamp(now(UTC).minusSeconds(70))
        ).forEach(statisticsService::add);
        //when
        Statistics statistics = statisticsService.get();
        //then
        assertThat(statistics.getCount().doubleValue()).isEqualTo(2);
        assertThat(statistics.getSum().doubleValue()).isEqualTo(47.51);
        assertThat(statistics.getMax().doubleValue()).isEqualTo(36.10);
        assertThat(statistics.getMin().doubleValue()).isEqualTo(11.41);
        assertThat(statistics.getAvg().doubleValue()).isEqualTo(23.755);
    }

    @Test
    public void should_getMin_when_firstLessThanSecond() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        BigDecimal first = new BigDecimal("0.02");
        BigDecimal second = new BigDecimal("0.37");
        //when
        BigDecimal min = statisticsService.getMin(first, second);
        //then
        assertThat(min).isEqualTo(new BigDecimal("0.02"));
    }

    @Test
    public void should_getMin_when_secondLessThanFirst() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        BigDecimal first = new BigDecimal("8.0");
        BigDecimal second = new BigDecimal("12.0");
        //when
        BigDecimal min = statisticsService.getMin(first, second);
        //then
        assertThat(min).isEqualTo(new BigDecimal("8.0"));
    }

    @Test
    public void should_getMin_when_secondIsZero() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        BigDecimal first = new BigDecimal("12.0");
        BigDecimal second = new BigDecimal("0.0");
        //when
        BigDecimal min = statisticsService.getMin(first, second);
        //then
        assertThat(min).isEqualTo(new BigDecimal("12.0"));
    }

    @Test
    public void should_getMin_when_firstIsZero() {
        //given
        StatisticsService statisticsService = new StatisticsService();
        BigDecimal first = new BigDecimal("0.0");
        BigDecimal second = new BigDecimal("12.0");
        //when
        BigDecimal min = statisticsService.getMin(first, second);
        //then
        assertThat(min).isEqualTo(new BigDecimal("12.0"));
    }

}