package com.n26.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.n26.model.Statistics;
import com.n26.model.Transaction;
import com.n26.service.StatisticsService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;

import static java.time.ZoneOffset.UTC;
import static java.util.Arrays.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static java.time.LocalDateTime.now;

@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class StatisticsControllerTest {

    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private StatisticsService statisticsService;

    @Before
    public void setup() {
        statisticsService.removeTransactions();
    }

    @Test
    public void should_get200WithEmptyStatistics_when_NoTransactions() throws Exception {
        //given
        String statistics = objectMapper.writeValueAsString(new Statistics());
        //when
        mockMvc.perform(get("/statistics")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(statistics));
    }

    @Test
    public void should_get200Statistics_when_TransactionsExists() throws Exception {
        //given
        asList(new Transaction().setAmount(new BigDecimal("122.455")).setTimestamp(now(UTC)),
                new Transaction().setAmount(new BigDecimal("100.155")).setTimestamp(now(UTC).minusSeconds(5))
        ).forEach(e -> statisticsService.add(e));

        //when
        Statistics expected = new Statistics()
                .setCount(new BigDecimal("2"))
                .setMax(new BigDecimal("122.46"))
                .setMin(new BigDecimal("100.16"))
                .setSum(new BigDecimal("222.61"))
                .setAvg(new BigDecimal("111.31"));
        String statistics = objectMapper.writeValueAsString(expected);

        mockMvc.perform(get("/statistics")
                .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string(statistics))
                .andReturn();
    }

}