package com.n26.controller;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.time.format.DateTimeFormatter;

import static com.n26.service.StatisticsService.TIME_FRAME_SECONDS;
import static java.time.Instant.*;
import static java.time.ZoneOffset.UTC;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
@RunWith(SpringRunner.class)
public class TransactionsControllerTest {

    public static final DateTimeFormatter TIMESTAMP_FORMATTER = DateTimeFormatter
            .ofPattern("yyyy-MM-dd'T'HH:mm:ss.SSSX").withZone(UTC);

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void should_get201WithEmptyBody_when_transactionIsValid() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().minusSeconds(TIME_FRAME_SECONDS - 10));
        String request = String.format("{\"amount\":\"%s\", \"timestamp\": \"%s\"}", "12.3343", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isCreated());

    }

    @Test
    public void should_get204WithEmptyBody_when_transactionOlderThanTimeFrame() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().minusSeconds(TIME_FRAME_SECONDS + 10));
        String request = String.format("{\"amount\":\"%s\", \"timestamp\": \"%s\"}", "12.3343", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isNoContent());
    }

    @Test
    public void should_get400WithEmptyBody_when_transactionIsInvalidJson() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().minusSeconds(TIME_FRAME_SECONDS - 10));
        String request = String.format("{amount\":\"%s\", \"timestamp\": \"%s\"}", "12.3343", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void should_get422WithEmptyBody_when_transactionParamsIsInvalid() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().minusSeconds(TIME_FRAME_SECONDS - 10));
        String request = String.format("{\"amount\":\"%s\", \"timestamp\": \"%s\"}", "12.3343eee", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnprocessableEntity())
                .andReturn();
    }

    @Test
    public void should_get422WithEmptyBody_when_transactionDateIsInTheFuture() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().plusSeconds(TIME_FRAME_SECONDS));
        String request = String.format("{\"amount\":\"%s\", \"timestamp\": \"%s\"}", "12.3343", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isUnprocessableEntity());
    }

    @Test
    public void should_get400WithEmptyBody_when_transactionParamsIsEmpty() throws Exception {
        //given
        String timestamp = TIMESTAMP_FORMATTER.format(now().minusSeconds(TIME_FRAME_SECONDS - 10));
        String request = String.format("{\"timestamp\": \"%s\"}", timestamp);

        //when
        mockMvc.perform(post("/transactions")
                .content(request)
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest())
                .andReturn();
    }

    @Test
    public void should_get204WithEmptyBody_when_removeAllTransaction() throws Exception {
        mockMvc.perform(delete("/transactions")
                .contentType(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(content().string(""))
                .andExpect(status().isNoContent());
    }

}
