package com.n26.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.n26.controller.editor.BigDecimalSerializer;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;

import static java.time.ZoneOffset.UTC;

@Data
@Accessors(chain = true)
public class Statistics {

    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal sum;
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal avg;
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal max;
    @JsonSerialize(using = BigDecimalSerializer.class)
    private BigDecimal min;
    private BigDecimal count;

    @JsonIgnore
    private LocalDateTime updated;

    public Statistics() {
        this.updated = LocalDateTime.now(UTC);
        this.sum = BigDecimal.ZERO;
        this.avg = BigDecimal.ZERO;
        this.max = BigDecimal.ZERO;
        this.min = BigDecimal.ZERO;
        this.count = BigDecimal.ZERO;
    }

    public static Statistics from(Transaction transaction) {
        return new Statistics()
                .setUpdated(transaction.getTimestamp())
                .setSum(transaction.getAmount())
                .setAvg(transaction.getAmount())
                .setMax(transaction.getAmount())
                .setMin(transaction.getAmount())
                .setCount(BigDecimal.ONE);
    }

    public Statistics withAverage() {
        if (this.getSum().doubleValue() > 0 && this.getCount().intValue() > 0) {
            this.avg = this.getSum().divide(this.getCount(), new MathContext(5));
        }
        return this;
    }
}
