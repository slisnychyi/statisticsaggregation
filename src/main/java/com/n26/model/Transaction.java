package com.n26.model;

import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;

import static org.springframework.format.annotation.DateTimeFormat.ISO.DATE_TIME;

@Data
@Accessors(chain = true)
public class Transaction {

    @NotNull
    private BigDecimal amount;
    @NotNull
    @DateTimeFormat(iso = DATE_TIME)
    private LocalDateTime timestamp;

}
