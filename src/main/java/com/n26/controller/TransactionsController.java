package com.n26.controller;

import com.n26.model.Transaction;
import com.n26.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.time.LocalDateTime;

import static com.n26.service.StatisticsService.TIME_FRAME_SECONDS;
import static java.time.ZoneOffset.UTC;
import static org.springframework.http.HttpStatus.*;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;
import static org.springframework.http.ResponseEntity.*;

@Slf4j
@RequiredArgsConstructor
@RestController("/transactions")
public class TransactionsController {

    private final StatisticsService statisticsService;

    @PostMapping(consumes = APPLICATION_JSON_VALUE)
    public ResponseEntity<Void> addTransaction(@RequestBody @Valid Transaction transaction) {
        log.info("Received transaction request to save for statistics [amount={}, timestamp={}]",
                transaction.getAmount(), transaction.getTimestamp());
        if (transaction.getTimestamp().isBefore(LocalDateTime.now(UTC).minusSeconds(TIME_FRAME_SECONDS))) {
            log.info("Transaction is older than 60 seconds.");
            return noContent().build();
        }
        if (transaction.getTimestamp().isAfter(LocalDateTime.now(UTC))) {
            log.info("Transaction is in the future.");
            return unprocessableEntity().build();
        }
        statisticsService.add(transaction);
        log.info("Transaction was saved.");
        return status(CREATED).build();

    }

    @DeleteMapping
    public ResponseEntity<Void> removeTransactions() {
        log.info("Received request to remove transactions");
        statisticsService.removeTransactions();
        return noContent().build();

    }
}