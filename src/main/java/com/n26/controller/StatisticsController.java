package com.n26.controller;

import com.n26.model.Statistics;
import com.n26.service.StatisticsService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@Slf4j
@RequiredArgsConstructor
@RestController("/statistics")
public class StatisticsController {

    private final StatisticsService statisticsService;

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public Statistics getStatistics() {
        log.info("Received request to get statistics.");
        return statisticsService.get();
    }

}