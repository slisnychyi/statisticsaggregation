package com.n26.service;

import com.n26.model.Statistics;
import com.n26.model.Transaction;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.Map;

import static com.n26.model.Statistics.from;
import static java.time.LocalDateTime.*;
import static java.time.ZoneOffset.UTC;
import static java.util.function.Function.*;
import static java.util.stream.Collectors.*;
import static java.util.stream.IntStream.*;

@Service
public class StatisticsService {

    public static final int TIME_FRAME_SECONDS = 60;

    private Map<Integer, Statistics> statistics;

    public StatisticsService() {
        this.statistics = getEmptyStatistics();
    }

    public void add(Transaction transaction) {
        int index = transaction.getTimestamp().getSecond();
        statistics.computeIfPresent(index, (key, statistics) -> isValid(statistics) ? addTransaction(transaction, statistics) : from(transaction));
    }

    public Statistics get() {
        return statistics.values().stream()
                .filter(this::isValid)
                .reduce(this::merge)
                .orElse(new Statistics());
    }

    public void removeTransactions() {
        this.statistics = getEmptyStatistics();
    }


    private Map<Integer, Statistics> getEmptyStatistics() {
        return rangeClosed(0, TIME_FRAME_SECONDS).boxed()
                .collect(toConcurrentMap(identity(), e -> new Statistics()));
    }


    private boolean isValid(Statistics statistics) {
        return statistics.getUpdated().isAfter(now(UTC).minusSeconds(TIME_FRAME_SECONDS));
    }

    private Statistics merge(Statistics first, Statistics second) {
        return new Statistics()
                .setSum(first.getSum().add(second.getSum()))
                .setMax(first.getMax().max(second.getMax()))
                .setMin(getMin(first.getMin(), second.getMin()))
                .setCount(first.getCount().add(second.getCount()))
                .withAverage();
    }

    BigDecimal getMin(BigDecimal first, BigDecimal second) {
        if (first.compareTo(second) > 0) {
            return second.doubleValue() > 0 ? second : first;
        }
        return first.doubleValue() > 0 ? first : second;
    }

    private Statistics addTransaction(Transaction transaction, Statistics statistics) {
        BigDecimal amount = transaction.getAmount();
        return new Statistics()
                .setUpdated(transaction.getTimestamp())
                .setSum(statistics.getSum().add(amount))
                .setMax(statistics.getMax().max(amount))
                .setMin(getMin(amount, statistics.getMin()))
                .setCount(statistics.getCount().add(BigDecimal.ONE));

    }
}